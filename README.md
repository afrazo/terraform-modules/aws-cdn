# Cloudfront CDN

Creates an AWS Cloudfront distribution using [Cloudposse's Cloudfront module](https://github.com/cloudposse/terraform-aws-cloudfront-s3-cdn), and handles all intertwined resources such as the S3 bucket, and TLS certificate.

## Usage

The following will create a Cloudfront distribution which:

* Will use `index.html` as the index page
* Have caching policies which will not cache `index.html`
* Create the alias "supersite.effingyeah.com" for the distribution
* Redirect to HTTPS
* Creates a TLS certificate using ACM, and uses it for the distribution (supplying a value for the `acm_certificate_arn` variable would stop this from happpening)
* Create the necessary permissions on the S3 bucket to be accessed from another account
* Set the allowed methods to `GET` and `HEAD`
* Sets a(n incomplete) content security policy

and a bunch more! See the [variable](https://gitlab.com/afrazo/terraform-modules/aws-cdn#inputs) defaults below for details.

```hcl
module "suerpsite" {
  source = "git::ssh://git@gitlab.com/afrazo/terraform-modules/aws-cdn.git?ref=0.0.1"

  name                     = "supersite"
  stage                    = "production"
  aliases                  = ["supersite.effingyeah.com"]
  parent_zone_name         = "effingyeah.com"
  cache_policy_id          = "658327ea-f89d-4fab-a63d-7e88639e58f6"
  origin_request_policy_id = "88a5eaf4-2fd4-4709-b370-b4c650ea3fcf"
  s3_requesting_roles      = ["arn:aws:iam::000000000000:role/cdn-deployer"]
  content_security_policy  = "default-src 'self'"
}
```

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.65.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 3.65.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_bucket_permissions"></a> [bucket\_permissions](#module\_bucket\_permissions) | ./cross-account-bucket-permissions | n/a |
| <a name="module_cdn_tagging"></a> [cdn\_tagging](#module\_cdn\_tagging) | git::https://github.com/cloudposse/terraform-null-label.git | 0.22.0 |
| <a name="module_certificate"></a> [certificate](#module\_certificate) | ./acm | n/a |
| <a name="module_lambda_at_edge"></a> [lambda\_at\_edge](#module\_lambda\_at\_edge) | cloudposse/cloudfront-s3-cdn/aws//modules/lambda@edge | 0.82.4 |
| <a name="module_this"></a> [this](#module\_this) | cloudposse/cloudfront-s3-cdn/aws | 0.79.0 |

## Resources

| Name | Type |
|------|------|
| [aws_waf_ipset.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/waf_ipset) | resource |
| [aws_waf_rule.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/waf_rule) | resource |
| [aws_waf_web_acl.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/waf_web_acl) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_acm_certificate_arn"></a> [acm\_certificate\_arn](#input\_acm\_certificate\_arn) | ARN of existing certificate to use. One is created if this is left blank | `any` | `null` | no |
| <a name="input_aliases"></a> [aliases](#input\_aliases) | List of DNS names to alias for this distribution | `list(any)` | n/a | yes |
| <a name="input_allowed_ips"></a> [allowed\_ips](#input\_allowed\_ips) | List of allowed IPs to the distribution. Defaults to 0.0.0.0/0 | `list(any)` | <pre>[<br>  "0.0.0.0/0"<br>]</pre> | no |
| <a name="input_allowed_methods"></a> [allowed\_methods](#input\_allowed\_methods) | List of verbs to allow. Defaults to GET and HEAD | `list(any)` | <pre>[<br>  "GET",<br>  "HEAD"<br>]</pre> | no |
| <a name="input_cache_policy_id"></a> [cache\_policy\_id](#input\_cache\_policy\_id) | Managed caching policy to use. Defaults to 658327ea-f89d-4fab-a63d-7e88639e58f6. See https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/using-managed-cache-policies.html | `string` | `"658327ea-f89d-4fab-a63d-7e88639e58f6"` | no |
| <a name="input_content_security_policy"></a> [content\_security\_policy](#input\_content\_security\_policy) | The CSP header to add | `string` | `"default-src 'none'; img-src 'self'; script-src 'self'; style-src 'self'; object-src 'none'"` | no |
| <a name="input_cors_allowed_methods"></a> [cors\_allowed\_methods](#input\_cors\_allowed\_methods) | List of verbs to allow via CORS. Defaults to GET | `list(any)` | <pre>[<br>  "GET"<br>]</pre> | no |
| <a name="input_default_ttl"></a> [default\_ttl](#input\_default\_ttl) | When to expire cache, in seconds. Defaults to 0 | `number` | `0` | no |
| <a name="input_index_pattern"></a> [index\_pattern](#input\_index\_pattern) | Pattern to point to the index page. Defaults to '/index.html' | `string` | `"/index.html"` | no |
| <a name="input_index_pattern_caching_policy"></a> [index\_pattern\_caching\_policy](#input\_index\_pattern\_caching\_policy) | Policy to use for caching of var.index\_pattern. Defaults to 4135ea2d-6df8-44a3-9df3-4b5a84be39ad. https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/using-managed-cache-policies.html | `string` | `"4135ea2d-6df8-44a3-9df3-4b5a84be39ad"` | no |
| <a name="input_max_ttl"></a> [max\_ttl](#input\_max\_ttl) | Maximum time cache can live, in seconds. Defaults to 0 | `number` | `0` | no |
| <a name="input_name"></a> [name](#input\_name) | Will be used throughout the module to name resources | `any` | n/a | yes |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | Used for tagging and naming | `any` | n/a | yes |
| <a name="input_origin_request_policy_id"></a> [origin\_request\_policy\_id](#input\_origin\_request\_policy\_id) | Managed request policy to use. Defaults to 88a5eaf4-2fd4-4709-b370-b4c650ea3fcf. See https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/using-managed-origin-request-policies.html | `string` | `"88a5eaf4-2fd4-4709-b370-b4c650ea3fcf"` | no |
| <a name="input_parent_zone_name"></a> [parent\_zone\_name](#input\_parent\_zone\_name) | Parent DNS for the records to add for var.aliases | `any` | n/a | yes |
| <a name="input_s3_requesting_roles"></a> [s3\_requesting\_roles](#input\_s3\_requesting\_roles) | Optional list of role ARNs that need write access to the bucket | `list` | `[]` | no |
| <a name="input_stage"></a> [stage](#input\_stage) | Used for tagging. e.g. staging, production, etc. | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_aliases"></a> [aliases](#output\_aliases) | Aliases of the CloudFront distribution |
| <a name="output_cf_arn"></a> [cf\_arn](#output\_cf\_arn) | ARN of AWS CloudFront distribution |
| <a name="output_cf_domain_name"></a> [cf\_domain\_name](#output\_cf\_domain\_name) | Domain name corresponding to the distribution |
| <a name="output_cf_primary_origin_id"></a> [cf\_primary\_origin\_id](#output\_cf\_primary\_origin\_id) | ID of the origin created by this module |
| <a name="output_s3_bucket"></a> [s3\_bucket](#output\_s3\_bucket) | Name of origin S3 bucket |
| <a name="output_s3_bucket_arn"></a> [s3\_bucket\_arn](#output\_s3\_bucket\_arn) | ARN of S3 origin S3 bucket |
| <a name="output_s3_bucket_domain_name"></a> [s3\_bucket\_domain\_name](#output\_s3\_bucket\_domain\_name) | Domain of origin S3 bucket |
