variable "aliases" {
  description = "List of DNS names to alias for this distribution"
  type        = list(any)
}

variable "parent_zone_name" {
  description = "Parent DNS for the records to add for var.aliases"
}

variable "tags" {
  description = "Tags to add to resources"
  default = {
    Module = "cdn"
  }
}
