This is unlikely to be useful elsewhere, but adding it as a module anyway to make it easier to move to a repo later if desired.

This module adds a policy to `var.assume_role` which will allow it to have full access to `var.buckets`.

What we're doing here in practice is allowing the Gitlab runners access to some buckets used for CloudFront publishing in our subaccounts, so that we can deploy to them from pipelines.

## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.crossaccount_bucket_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_policy_document.assume_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_bucket"></a> [bucket](#input\_bucket) | Bucket to give access to | `string` | n/a | yes |
| <a name="input_requesting_roles"></a> [requesting\_roles](#input\_requesting\_roles) | Role from other account that should be given access | `list(string)` | n/a | yes |

## Outputs

No outputs.
