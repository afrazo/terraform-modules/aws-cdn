data "aws_iam_policy_document" "assume_policy" {
  dynamic "statement" {
    for_each = local.requesting_account_ids

    content {
      actions = ["sts:AssumeRole"]
      effect  = "Allow"

      principals {
        identifiers = [statement.value]
        type        = "AWS"
      }
    }
  }
}

resource "aws_iam_role" "crossaccount_bucket_access" {
  name               = "${var.bucket}-crossaccount-s3-access"
  description        = "Rights needed for Gitlab runners to access ${var.bucket}"
  assume_role_policy = data.aws_iam_policy_document.assume_policy.json
}

data "aws_iam_policy_document" "this" {
  statement {
    actions = [
      "s3:*"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:s3:::${var.bucket}",
      "arn:aws:s3:::${var.bucket}/*"
    ]
  }
}

resource "aws_iam_policy" "this" {
  name        = "${var.bucket}-management-access"
  path        = "/"
  description = "Allows access to the ${var.bucket} bucket"

  policy = data.aws_iam_policy_document.this.json
}

resource "aws_iam_role_policy_attachment" "this" {
  role       = aws_iam_role.crossaccount_bucket_access.name
  policy_arn = aws_iam_policy.this.arn
}
