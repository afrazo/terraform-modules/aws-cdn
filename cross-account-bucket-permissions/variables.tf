variable "requesting_roles" {
  description = "Role from other account that should be given access"
  type        = list(string)
}

variable "bucket" {
  description = "Bucket to give access to"
  type        = string
}

locals {
  requesting_account_ids = [for id in var.requesting_roles : regex("arn:aws:iam::([0-9]+)", id)[0]]
}
