module "cdn_tagging" {
  source    = "git::https://github.com/cloudposse/terraform-null-label.git?ref=0.22.0"
  namespace = var.namespace
  name      = var.name

  tags = {
    "Terraform" = "true"
    "Module"    = "cloudposse/cdn"
  }
}

module "certificate" {
  count     = var.acm_certificate_arn == null ? 1 : 0
  providers = { aws = aws.us-east-1 }

  source = "./acm"

  aliases          = var.aliases
  parent_zone_name = var.parent_zone_name
  tags             = module.cdn_tagging.tags
}

resource "aws_waf_ipset" "this" {
  name = "${var.name}-ipset"

  dynamic "ip_set_descriptors" {
    for_each = var.allowed_ips

    content {
      type  = "IPV4"
      value = ip_set_descriptors.value
    }
  }
}

resource "aws_waf_rule" "this" {
  depends_on  = [aws_waf_ipset.this]
  name        = "${var.name}-ipset-allow"
  metric_name = local.aws_waf_rule_metric_name

  predicates {
    data_id = aws_waf_ipset.this.id
    negated = false
    type    = "IPMatch"
  }
}

resource "aws_waf_web_acl" "this" {
  depends_on = [
    aws_waf_ipset.this,
    aws_waf_rule.this,
  ]

  name        = "${var.name}-acl"
  metric_name = local.aws_waf_web_acl_metric_name

  default_action {
    type = "BLOCK"
  }

  rules {
    action {
      type = "ALLOW"
    }

    priority = 1
    rule_id  = aws_waf_rule.this.id
    type     = "REGULAR"
  }
}

###########

module "lambda_at_edge" {
  source  = "cloudposse/cloudfront-s3-cdn/aws//modules/lambda@edge"
  version = "0.82.4"

  providers = { aws = aws.us-east-1 }

  functions = {
    origin_request = {
      source = [{
        content  = <<-EOT
        'use strict';

        exports.handler = (event, context, callback) => {

          //Get contents of response
          const response = event.Records[0].cf.response;
          const headers = response.headers;

          //Set new headers
          headers['strict-transport-security'] = [{key: 'Strict-Transport-Security', value: 'max-age=63072000; includeSubdomains; preload'}];
          headers['content-security-policy'] = [{key: 'Content-Security-Policy', value: "${var.content_security_policy}"}];
          headers['x-content-type-options'] = [{key: 'X-Content-Type-Options', value: 'nosniff'}];
          headers['x-frame-options'] = [{key: 'X-Frame-Options', value: 'DENY'}];
          headers['x-xss-protection'] = [{key: 'X-XSS-Protection', value: '1; mode=block'}];
          headers['referrer-policy'] = [{key: 'Referrer-Policy', value: 'same-origin'}];

          //Return modified response
          callback(null, response);
        };
        EOT
        filename = "index.js"
      }]
      runtime      = "nodejs12.x"
      handler      = "index.handler"
      event_type   = "origin-response"
      include_body = false
    }
  }
}

###########

module "this" {
  source  = "cloudposse/cloudfront-s3-cdn/aws"
  version = "0.79.0"

  namespace                   = "supercompany"
  stage                       = var.stage
  name                        = var.name
  aliases                     = var.aliases
  dns_alias_enabled           = true
  parent_zone_name            = var.parent_zone_name
  acm_certificate_arn         = var.acm_certificate_arn != null ? var.acm_certificate_arn : module.certificate[0].aws_acm_certificate_arn
  additional_tag_map          = module.cdn_tagging.tags
  allowed_methods             = var.allowed_methods
  cors_allowed_methods        = var.cors_allowed_methods
  cache_policy_id             = var.cache_policy_id
  origin_request_policy_id    = var.origin_request_policy_id
  default_ttl                 = var.default_ttl
  max_ttl                     = var.max_ttl
  lambda_function_association = module.lambda_at_edge.lambda_function_association
  # web_acl_id               = aws_waf_web_acl.this.id

  custom_error_response = [{
    error_caching_min_ttl = "10",
    error_code            = "404",
    response_code         = "200",
    response_page_path    = var.index_pattern
  }]

  ordered_cache = [{
    target_origin_id                  = "${var.namespace}-${var.stage}-${var.name}"
    path_pattern                      = var.index_pattern
    allowed_methods                   = var.allowed_methods
    cached_methods                    = var.allowed_methods
    compress                          = true
    cache_policy_id                   = var.index_pattern_caching_policy
    origin_request_policy_id          = var.origin_request_policy_id
    min_ttl                           = 0
    default_ttl                       = 0
    max_ttl                           = 0
    forward_query_string              = true
    forward_header_values             = []
    forward_cookies                   = "all"
    lambda_function_association       = module.lambda_at_edge.lambda_function_association
    function_association              = []
    trusted_key_groups                = []
    trusted_signers                   = []
    viewer_protocol_policy            = "redirect-to-https"
    forward_cookies_whitelisted_names = []
  }]
}

module "bucket_permissions" {
  source = "./cross-account-bucket-permissions"

  requesting_roles = var.s3_requesting_roles
  bucket           = "${var.namespace}-${var.stage}-${var.name}-origin"
}
