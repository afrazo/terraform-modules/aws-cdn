output "s3_bucket_domain_name" {
  description = "Domain of origin S3 bucket"
  value       = module.this.s3_bucket_domain_name
}

output "s3_bucket_arn" {
  description = "ARN of S3 origin S3 bucket"
  value       = module.this.s3_bucket_arn
}

output "s3_bucket" {
  description = "Name of origin S3 bucket"
  value       = module.this.s3_bucket
}

output "cf_primary_origin_id" {
  description = "ID of the origin created by this module"
  value       = module.this.cf_primary_origin_id
}

output "cf_domain_name" {
  description = "Domain name corresponding to the distribution"
  value       = module.this.cf_domain_name
}

output "cf_arn" {
  description = "ARN of AWS CloudFront distribution"
  value       = module.this.cf_arn
}

output "aliases" {
  description = "Aliases of the CloudFront distribution"
  value       = module.this.aliases
}
