variable "namespace" {
  description = "Used for tagging and naming"
}

variable "name" {
  description = "Will be used throughout the module to name resources"
}

variable "stage" {
  description = "Used for tagging. e.g. staging, production, etc."
}

variable "aliases" {
  description = "List of DNS names to alias for this distribution"
  type        = list(any)
}

variable "parent_zone_name" {
  description = "Parent DNS for the records to add for var.aliases"
}

variable "acm_certificate_arn" {
  description = "ARN of existing certificate to use. One is created if this is left blank"
  default     = null
}

variable "allowed_methods" {
  description = "List of verbs to allow. Defaults to GET and HEAD"
  type        = list(any)
  default     = ["GET", "HEAD"]
}

variable "cors_allowed_methods" {
  description = "List of verbs to allow via CORS. Defaults to GET"
  type        = list(any)
  default     = ["GET"]
}

variable "cache_policy_id" {
  description = "Managed caching policy to use. Defaults to 658327ea-f89d-4fab-a63d-7e88639e58f6. See https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/using-managed-cache-policies.html"
  default     = "658327ea-f89d-4fab-a63d-7e88639e58f6"
}

variable "origin_request_policy_id" {
  description = "Managed request policy to use. Defaults to 88a5eaf4-2fd4-4709-b370-b4c650ea3fcf. See https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/using-managed-origin-request-policies.html"
  default     = "88a5eaf4-2fd4-4709-b370-b4c650ea3fcf"
}

variable "default_ttl" {
  description = "When to expire cache, in seconds. Defaults to 0"
  default     = 0
}

variable "max_ttl" {
  description = "Maximum time cache can live, in seconds. Defaults to 0"
  default     = 0
}

variable "index_pattern" {
  description = "Pattern to point to the index page. Defaults to '/index.html'"
  default     = "/index.html"
}

variable "index_pattern_caching_policy" {
  description = "Policy to use for caching of var.index_pattern. Defaults to 4135ea2d-6df8-44a3-9df3-4b5a84be39ad. https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/using-managed-cache-policies.html"
  default     = "4135ea2d-6df8-44a3-9df3-4b5a84be39ad"
}

variable "s3_requesting_roles" {
  description = "Optional list of role ARNs that need write access to the bucket"
  default     = []
}

variable "allowed_ips" {
  description = "List of allowed IPs to the distribution. Defaults to 0.0.0.0/0"
  type        = list(any)
  default     = ["0.0.0.0/0"]
}

variable "content_security_policy" {
  description = "The CSP header to add"
  default     = "default-src 'none'; img-src 'self'; script-src 'self'; style-src 'self'; object-src 'none'"
}

locals {
  aws_waf_rule_metric_name    = replace("${var.name}-ipset-allow", "-", "")
  aws_waf_web_acl_metric_name = replace("${var.name}-acl", "-", "")
}
